export const defaultconfigdata = {
    
    // About us content Starts
    about_us: 'Centurion Infotech is a young, dynamic & next-generation IT company based in Bardoli, Gujarat, India. We understand that businesses today heavily rely on seamless technology to remain productive and competitive, and we precisely ensure that. We have been offering our result-oriented services to the growing industrial world by our experienced professionals. We have worked with several clients and offer reliable creations to businesses belonging to different sectors. Our selective team of tech experts has years of experience in website & application development, technology implementation and strategic consulting, as well as proactive development support.',
    // About us content ends

    // Top Cards Constant Starts
    Inactive_users_label: 'In-Active Users',
    Active_users_label: 'Active Users',
    Total_users_label: 'Total Users',
    Distributors_label: 'Distributors',
    Agents_label: 'Agents',
    Total_Sales_label: 'Total Sales',
    // Top Cards Constant Ends

    // Menu items File Constant Starts
    dashboard: 'Dashboard',
    distributors: 'Distributors',
    agents: 'Agents',
    superadmins: 'Super Admins',
    clients: 'Clients',
    aboutus: 'About Us',
    needhelp: 'Need Help?',
    sitesettings: 'Site Settings',
    // Menu items File Constant Ends

    //Sales Summary File Constant Starts
    salessummary: 'Sales Summary',
    yrsalessummary: 'Yearly Sales Summary',
    monsalessummary: 'Monthly Sales Summary',
    weeksalessummary: 'Weekly Sales Summary',
    dailysalessummary: 'Daily Sales Summary',
    //Sales Summary File Constant Ends

    //Feed Card File Constant starts
    dailyact: 'Daily Activity',
    dailynot: 'Notifications you have',
    //Feed Card File Constant ends

    //Feed Card File Constant starts
    topsell: 'Top Selling',
    overviewofp: 'Ovierview of Projects',
    //Feed Card File Constant ends

    //Distributors File Constant starts
    listofdist: 'List of Distributors',
    //Distributors File Constant ends 
    
    //Distributors File Constant starts
    listofagent: 'List of Agents',
    //Distributors File Constant ends
    
    //Distributors File Constant starts
    listofsadmin: 'List of Super Admins',
    //Distributors File Constant ends 
    
    //Distributors File Constant starts
    listofclients: 'List of Clients',
    //Distributors File Constant ends

    //Add-client form File Constant starts

    //stepper1 File Constant starts
    stepper1_label: 'Company Information',
    company_name: 'Company Name*',
    website: 'Website',
    mobile: 'Mobile*',
    phone: 'Phone',
    email: 'Email*',
    fax: 'Fax',
    type_of_company: 'Type of Company',
    company_active_since: 'Company Active since*',
    company_size: 'Company Size',
    division: 'Division',
    //stepper1 File Constant ends

    //stepper2 File Constant starts
    stepper2_label: 'Address Information',
    company_status: 'Company Status*',
    active: 'Active',
    inactive: 'Inactive',
    address: 'Address',
    state: 'State*',
    zip_code: 'Zip Code*',
    country: 'Country*',
    whatsapp: 'Whatsapp',
    twitter: 'Twitter',
    instagram: 'Instagram',
    facebook: 'Facebook',
     //stepper2 File Constant ends

    //stepper3 File Constant starts
    stepper3_label: 'Contact Person Information',
    contact_person_name: 'Contact Person Name*',
    contact_person_title: 'Contact Person Title',
    contact_person_email: 'Contact Person Email*',
    contact_person_mobile: 'Contact Person Mobile*',
    contact_person_phone: 'Contact Person Phone',
    contact_person_whatsapp: 'Contact Person Whatsapp',
    contact_person_twitter: 'Contact Person Twitter',
    contact_person_instagram: 'Contact Person Instagram',
    contact_person_facebook: 'Contact Person Facebook',
     //stepper3 File Constant ends

    //stepper4 File Constant starts
    stepper4_label: 'Billing Information',
    select_plan: 'Select Plan*',
    basic: 'Basic',
    standard: 'Standard',
    pro: 'Pro',
    executive: 'Executive',
    enterprise: 'Enterprise',
    billing_person_name: 'Billing Person Name*',
    billing_person_email: 'Billing Person Email*',
    billing_person_mobile: 'Billing Person Mobile*',
    billing_person_phone: 'Billing Person Phone',
    billing_person_address: 'Billing Person Address',
    billing_person_city: 'Billing Person City',
    billing_person_state: 'Billing Person State',
    billing_person_zip_code: 'Billing Person Zip Code',
     //stepper4 File Constant ends

     //Buttons in steppers File Constant starts
     Next: 'Next',
     Previous: 'Previous',
     submit: 'Submit',
     apiurl : 'https://ci-managementtool.000webhostapp.com/'
    //Buttons in steppers File Constant ends

    //Add-client form File Constant ends






}