import { Component, OnInit, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { defaultconfigdata } from '../../../environments/configdata';
import Stepper from 'bs-stepper';
import Swal from 'sweetalert2';
// import ngx-translate and the http loader
import {TranslateLoader, TranslateModule, TranslateService} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import { Router } from '@angular/router'

// import custom validator to validate that password and confirm password fields match


@Component({
    selector: 'Superadmin',
    templateUrl: './signup.html',
    styleUrls: ['../login_signup/css/style.css']
})
export class signupcomponent implements AfterViewInit {

    signupnval: FormGroup;
   

    submitted = false;

    constructor(private formBuilder: FormBuilder, public translate: TranslateService, private route: Router) { 
      translate.setDefaultLang('en');
    }
    login()
    {
      this.route.navigate(['/superadmin/login'])
    }
  
    ngOnInit() {
        this.signupnval = this.formBuilder.group({
            full_name: ['', Validators.required],
            username: ['', Validators.required],
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.minLength(6)]],
        });
    }
    ngAfterViewInit() {
        
    }

    // convenience getter for easy access to form fields
    get f() { return this.signupnval.controls; }
    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.signupnval.invalid) {
            return;
        }

        // display form values on success
        this.route.navigate(['./superadmin/login'])
    }

}
