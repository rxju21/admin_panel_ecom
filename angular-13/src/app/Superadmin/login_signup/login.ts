import { Component, OnInit, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { defaultconfigdata } from '../../../environments/configdata';
import Stepper from 'bs-stepper';
import Swal from 'sweetalert2';
// import ngx-translate and the http loader
import {TranslateLoader, TranslateModule, TranslateService} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import { Router } from '@angular/router'

// import custom validator to validate that password and confirm password fields match


@Component({
    selector: 'Superadmin',
    templateUrl: './login.html',
    styleUrls: ['../login_signup/css/style.css']
})
export class logincomponent implements AfterViewInit {

    loginval: FormGroup;
   

    submitted = false;

    constructor(private http: HttpClient,private formBuilder: FormBuilder, public translate: TranslateService, private route: Router) { 
      translate.setDefaultLang('en');
    }
    ngOnInit() {

        this.http.post<any>('https://test-dev.rovuk.us:4201/webapi/v1/login',
            {
                "email": 'cirajjb@gmail.com',
                "password": 'centurion1#'
            }).subscribe(data => {
                console.log("data:",data)
            }, error => {
                console.log("error:",error)
            });

        this.loginval = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.minLength(6)]],
            select_role: ['', Validators.required],
           
        });
    }
    ngAfterViewInit() {
        
    }

    // convenience getter for easy access to form fields
    get f() { return this.loginval.controls; }
    onSubmit() 
    {
        this.submitted = true;

        // stop here if form is invalid
        if (this.loginval.invalid) {
            return;
        }

        // display form values on success
        this.route.navigate(['/superadmin/dashboard'])
    }
    signup() 
    {
        // display form values on success
        this.route.navigate(['/superadmin/signup'])
    }

}
