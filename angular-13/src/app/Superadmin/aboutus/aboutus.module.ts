import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { AboutusComponent } from "./aboutus.component";

const routes: Routes = [
  {
    path: "",
    data: {
      title: "Aboutus",
      urls: [{ title: "Aboutus", url: "/aboutus" }, { title: "Aboutus" }],
    },
    component: AboutusComponent,
  },
];

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    RouterModule.forChild(routes),
  ],
  declarations: [
  ],
})
export class AboutModule {}
