import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import {  DistributorComponent } from "./distributor.component";


const routes: Routes = [
  {
    path: "",
    data: {
      title: "Demo",
      urls: [{ title: "Demo", url: "/distributor" }, { title: "Demo" }],
    },
    component: DistributorComponent,
  },
];

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    RouterModule.forChild(routes),
  ],
  declarations: [
    
  ],
})
export class demoModule_ {}
