import { Component } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Product, TopSelling } from './distributors.data';
import { defaultconfigdata } from '../../../environments/configdata';

@Component({
  selector: 'distributor',
  templateUrl: './distributor.component.html',
})
export class DistributorComponent
{

  listofdist: any = defaultconfigdata.listofdist;

  topSelling: Product[];

  trow: Product[];

  constructor(private modalService: NgbModal) {

    this.topSelling = TopSelling;

    this.trow = TopSelling;
  }


    // openBackDropCustomClass(content:any) 
    // {
    //   this.modalService.open(content, { backdropClass: 'light-blue-backdrop' });
      
    // }
    open()
      {
        // this.modalService.open(content, { size: 'lg' });
        console.log("Open Dailog Here");
      }
  }

