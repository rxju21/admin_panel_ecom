import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { AgentComponent } from "./agent.component";


const routes: Routes = [
  {
    path: "",
    data: {
      title: "Agents",
      urls: [{ title: "Agents", url: "/agent" }, { title: "Agents" }],
    },
    component: AgentComponent,
  },
];

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    RouterModule.forChild(routes),
  ],
  declarations: [

  ],
})
export class AgentdemoModule_ { }
