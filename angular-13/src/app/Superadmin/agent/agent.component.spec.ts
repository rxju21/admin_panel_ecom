import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoAgentComponent } from './agent.component';

describe('DemoAgentComponent', () => {
  let component: DemoAgentComponent;
  let fixture: ComponentFixture<DemoAgentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DemoAgentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoAgentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
