import { Component } from '@angular/core';
import {Product,TopSelling,} from './agents.data';
import { defaultconfigdata } from '../../../environments/configdata';


@Component({
    selector: 'app-table',
    templateUrl: './agent.component.html'
})
export class AgentComponent {

  listofagent:any = defaultconfigdata.listofagent;


  topSelling:Product[];

  trow:Product[];

  constructor() { 

    this.topSelling=TopSelling;

    this.trow=TopSelling;
  }
}
