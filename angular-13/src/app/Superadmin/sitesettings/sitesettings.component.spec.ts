import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoSitesettingsComponent } from './sitesettings.component';

describe('DemoSitesettingsComponent', () => {
  let component: DemoSitesettingsComponent;
  let fixture: ComponentFixture<DemoSitesettingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DemoSitesettingsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoSitesettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
