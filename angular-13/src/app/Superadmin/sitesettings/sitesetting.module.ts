import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { sitesettingsComponent } from "./sitesettings.component";


const routes: Routes = [
  {
    path: "",
    data: {
      title: "Site Settings",
      urls: [{ title: "Site Settings", url: "/sitesettings" }, { title: "Site Settings" }],
    },
    component: sitesettingsComponent,
  },
];

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    RouterModule.forChild(routes),
  ],
  declarations: [
    
  ],
})
export class DemoSitesettingsdemoModule_ {}
