export interface Product {
    image: string,
    uname: string,
    gmail: string,
    productName: string,
    status: string,
    weeks: number,
    budget: string,
    fname: string,
    lname: string,
    u_id: string,
}

export const TopSelling: Product[] = [

    {
        image: 'assets/images/users/user1.jpg',
        uname: 'SakshiSingh Dhoni',
        gmail: 'sakshi_mahi@gmail.com',
        productName: 'Flexy React',
        status: 'success',
        weeks: 35,
        budget: '95K',
        fname: "Sakshi",
        lname: "Dhoni",
        u_id: "@sakshisingh.dhoni",
    },
    {
        image: 'assets/images/users/user2.jpg',
        uname: 'Deepak Chahar',
        gmail: 'dchahar@gmail.com',
        productName: 'Landing Pro React',
        status: 'warning',
        weeks: 35,
        budget: '95K',
        fname: "Deepak",
        lname: "Chahar",
        u_id: "@deepak.chahar14",
    },
    {
        image: 'assets/images/users/user3.jpg',
        uname: 'Ritika Rohit Sharma',
        gmail: 'rrsharma@gmail.com',
        productName: 'Elite React	',
        status: 'danger',
        weeks: 35,
        budget: '95K',
        fname: "Ritika",
        lname: "Sajdeh",
        u_id: "@ritika.sajdeh",
    },
    {
        image: 'assets/images/users/user4.jpg',
        uname: 'Virat Kohli',
        gmail: 'iamvkohli@gmail.com',
        productName: 'Ample React',
        status: 'info',
        weeks: 35,
        budget: '95K',
        fname: "Virat",
        lname: "Kohli",
        u_id: "@virat.kohli",
    },

]