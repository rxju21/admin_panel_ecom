import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Product, TopSelling } from './clients.data';
import { defaultconfigdata } from '../../../environments/configdata';



@Component({
  selector: 'app-client',
  templateUrl: './client.component.html'
})
export class ClientComponent {

  
  listofclients: any = defaultconfigdata.listofclients;

  topSelling: Product[];

  trow: Product[];

  constructor(private route: Router) {

    this.topSelling = TopSelling;

    this.trow = TopSelling;
  }
   createclient() {
    this.route.navigate(['/superadmin/AddNewClients']);
  }
}
