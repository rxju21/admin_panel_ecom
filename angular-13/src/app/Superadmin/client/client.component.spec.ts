import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoClientComponent } from './client.component';

describe('DemoClientComponent', () => {
  let component: DemoClientComponent;
  let fixture: ComponentFixture<DemoClientComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DemoClientComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
