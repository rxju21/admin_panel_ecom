import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { ClientComponent } from "./client.component";


const routes: Routes = [
  {
    path: "",
    data: {
      title: "Clients",
      urls: [{ title: "Clients", url: "/client" }, { title: "Clients" }],
    },
    component: ClientComponent,
  },
];

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    RouterModule.forChild(routes),
  ],
  declarations: [
    
  ],
})
export class ClientdemoModule_ {}
