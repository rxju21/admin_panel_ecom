import { RouteInfo } from './sidebar.metadata';
import { defaultconfigdata } from '../../../../environments/configdata';

export const ROUTES: RouteInfo[] = [
 
  {
    path: '/dashboard',
    title: defaultconfigdata.dashboard,
    icon: 'bi bi-speedometer2',
    class: '',
    extralink: false,
    submenu: []
  },
  {
    path: '/demo',
    title: "Distributors",
    icon: 'bi bi-bell',
    class: '',
    extralink: false,
    submenu: []
  },
  {
    path: '/demo-agent',
    title: defaultconfigdata.agents,
    icon: 'bi bi-people-fill',
    class: '',
    extralink: false,
    submenu: []
  },
  {
    path: '/demo-superadmin',
    title: defaultconfigdata.superadmins,
    icon: 'bi bi-person-square',
    class: '',
    extralink: false,
    submenu: []
  },
  
  {
    path: '/demo-client',
    title: defaultconfigdata.clients,
    icon: 'bi bi-person-rolodex',
    class: '',
    extralink: false,
    submenu: []
  },
  {
    path: '/about',
    title: defaultconfigdata.aboutus,
    icon: 'bi bi-people',
    class: '',
    extralink: false,
    submenu: []
  },
  {
    path: '/demo-needhelp',
    title: 'Need Help?',
    icon: 'bi bi-info-square-fill',
    class: '',
    extralink: false,
    submenu: []
  },
  {
    path: '/demo-sitesettings',
    title: 'Site Settings',
    icon: 'bi bi-gear-wide-connected',
    class: '',
    extralink: false,
    submenu: []
  },
];
