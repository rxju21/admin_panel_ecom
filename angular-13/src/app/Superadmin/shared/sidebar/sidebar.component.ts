import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit } from '@angular/core';
import { RouteInfo } from './sidebar.metadata';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html'
})
export class SidebarComponent implements OnInit {
  showMenu = '';
  showSubMenu = '';
  public sidebarnavItems:RouteInfo[]=[];
  // this is for the open close
  addExpandClass(element: string) {
    if (element === this.showMenu) {
      this.showMenu = '0';
    } else {
      this.showMenu = element;
    }
  }

  constructor(
    private modalService: NgbModal,
    private router: Router,
    private route: ActivatedRoute,
    public translate: TranslateService
    ) {this.translate.stream(['']).subscribe((textarray) => {

      this.sidebarnavItems= [
 
        {
          path: '/superadmin/dashboard',
          title: this.translate.instant('sidebar_dashboard.title'),
          icon: 'bi bi-speedometer2',
          class: '',
          extralink: false,
          submenu: []
        },
        {
          path: '/superadmin/Distributors',
          title: this.translate.instant('sidebar_distributors.title'),
          icon: 'bi bi-bell',
          class: '',
          extralink: false,
          submenu: []
        },
        {
          path: '/superadmin/Agents',
          title: this.translate.instant('sidebar_agents.title'),
          icon: 'bi bi-people-fill',
          class: '',
          extralink: false,
          submenu: []
        },
        {
          path: '/superadmin/Superadmins',
          title: this.translate.instant('sidebar_superadmin.title'),
          icon: 'bi bi-person-square',
          class: '',
          extralink: false,
          submenu: []
        },
        
        {
          path: '/superadmin/Clients',
          title: this.translate.instant('sidebar_client.title'),
          icon: 'bi bi-person-rolodex',
          class: '',
          extralink: false,
          submenu: []
        },
        {
          path: '/superadmin/Aboutus',
          title: this.translate.instant('sidebar_Aboutus.title'),
          icon: 'bi bi-people',
          class: '',
          extralink: false,
          submenu: []
        },
        {
          path: '/superadmin/NeedHelp',
          title: this.translate.instant('sidebar_needhelp.title'),
          icon: 'bi bi-info-square-fill',
          class: '',
          extralink: false,
          submenu: []
        },
        {
          path: '/superadmin/SiteSettings',
          title: this.translate.instant('sidebar_sitesettings.title'),
          icon: 'bi bi-gear-wide-connected',
          class: '',
          extralink: false,
          submenu: []
        },
      ];


    });
  }

  // End open close
  ngOnInit() {
    //this.sidebarnavItems = ROUTES.filter(sidebarnavItem => sidebarnavItem);
  }
}
