import { Component, OnInit } from '@angular/core';
import {Product,TopSelling} from './top-selling-data';
import { defaultconfigdata } from '../../../../../environments/configdata';
// import { defaultconfigdata } from 'C:/Users/saura/Documents/Internship2022/ang/admin_panel_ecom/angular-13/src/environments/configdata';

@Component({
  selector: 'app-top-selling',
  templateUrl: './top-selling.component.html'
})
export class TopSellingComponent implements OnInit {

  topsell: any = defaultconfigdata.topsell;
  overviewofp: any = defaultconfigdata.overviewofp;


  topSelling:Product[];

  constructor() { 

    this.topSelling=TopSelling;
  }

  ngOnInit(): void {
  }

}
