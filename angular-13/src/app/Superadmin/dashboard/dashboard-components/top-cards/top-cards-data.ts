export interface topcard {
    bgcolor: string,
    icon: string,
    title: string,
    subtitle: string
}

export const topcards: topcard[] = [

    {
        bgcolor: 'success',
        icon: 'bi bi-person-check-fill',
        title: '10',
        subtitle: 'Distributors'
    },
    {
        bgcolor: 'danger',
        icon: 'bi bi-person-check',
        title: '20',
        subtitle: 'Agents'
    },
    {
        bgcolor: 'success',
        icon: 'bi bi-people',
        title: '660',
        subtitle: 'Total Users'
    },
    {
        bgcolor: 'warning',
        icon: 'bi bi-people-fill',
        title: '450',
        subtitle: 'Active Users'
    },
    {
        bgcolor: 'info',
        icon: 'bi bi-person-x-fill',
        title: '210',
        subtitle: 'Inactive Users',
    },
    {
        bgcolor: 'warning',
        icon: 'bi bi-cash-coin',
        title: '750K',
        subtitle: 'Total Sales'
    },

] 