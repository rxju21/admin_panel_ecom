import { topcard } from './top-cards-data';
import { Component, OnInit } from '@angular/core';
import { defaultconfigdata } from '../../../../../environments/configdata';
import { TranslateService } from '@ngx-translate/core';
// import { defaultconfigdata } from 'C:/Users/saura/Documents/Internship2022/ang/admin_panel_ecom/angular-13/src/environments/configdata';

@Component({
  selector: 'app-top-cards',
  templateUrl: './top-cards.component.html'
})
export class TopCardsComponent implements OnInit {

  inactive_users: any = defaultconfigdata.Inactive_users_label;
  active_users: any = defaultconfigdata.Active_users_label;
  total_users: any = defaultconfigdata.Total_users_label;
  distributors: any = defaultconfigdata.Distributors_label;
  agents: any = defaultconfigdata.Agents_label;
  totalsales: any = defaultconfigdata.Total_Sales_label;

  topcards: topcard[];

  constructor(public translate: TranslateService) {
    this.translate.stream(['']).subscribe((textarray) => 
    {
      this.topcards = [
 
        {
          bgcolor: 'success',
          icon: 'bi bi-person-check-fill',
          title: '10',
          subtitle: this.translate.instant('topcards_dashboard.title'),
      },
      {
          bgcolor: 'danger',
          icon: 'bi bi-person-check',
          title: '20',
          subtitle: this.translate.instant('topcards_agents.title'),
      },
      {
          bgcolor: 'success',
          icon: 'bi bi-people',
          title: '660',
          subtitle: this.translate.instant('topcards_totalusers.title'),
      },
      {
          bgcolor: 'warning',
          icon: 'bi bi-people-fill',
          title: '450',
          subtitle: this.translate.instant('topcards_activeusers.title'),
      },
      {
          bgcolor: 'info',
          icon: 'bi bi-person-x-fill',
          title: '210',
          subtitle: this.translate.instant('topcards_inactiveusers.title'),
      },
      {
          bgcolor: 'warning',
          icon: 'bi bi-cash-coin',
          title: '750K',
          subtitle: this.translate.instant('topcards_totalsales.title'),
      },

      ];
    }
  )};

  ngOnInit(): void {
  }

}
