import { Component, OnInit, ViewChild } from '@angular/core';
import {
  ApexAxisChartSeries,
  ApexChart,
  ChartComponent,
  ApexDataLabels,
  ApexYAxis,
  ApexLegend,
  ApexXAxis,
  ApexTooltip,
  ApexTheme,
  ApexGrid
} from 'ng-apexcharts';
import { defaultconfigdata } from '../../../../../environments/configdata';
import {TranslateLoader, TranslateModule, TranslateService} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient, HttpClientModule} from '@angular/common/http';
// import { defaultconfigdata } from 'C:/Users/saura/Documents/Internship2022/ang/admin_panel_ecom/angular-13/src/environments/configdata';

export type salesChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  yaxis: ApexYAxis;
  stroke: any;
  theme: ApexTheme;
  tooltip: ApexTooltip;
  dataLabels: ApexDataLabels;
  legend: ApexLegend;
  colors: string[];
  markers: any;
  grid: ApexGrid;
};

@Component({
  selector: 'app-sales-summary',
  templateUrl: './sales-summary.component.html'
})
export class SalesSummaryComponent implements OnInit {
  
  salessummary: any = defaultconfigdata.salessummary;
  yearlysalessummary: any = defaultconfigdata.yrsalessummary;
  monthlysalessummary: any = defaultconfigdata.monsalessummary;
  weeklysalessummary: any = defaultconfigdata.weeksalessummary;
  dailysalessummary: any = defaultconfigdata.dailysalessummary;

  @ViewChild("chart") chart: ChartComponent = Object.create(null);
  public salesChartOptions: Partial<salesChartOptions>;
  
  
  constructor(public translate: TranslateService) { 
    translate.setDefaultLang('en');
    
  
    this.salesChartOptions = {
      series: [
        {
          name: "Active Users",
          data: [0, 31, 40, 330, 51, 42, 109, 620],
        },
        {
          name: "Inactive Users",
          data: [0, 11, 32, 330, 32, 34, 52, 40],
        },
      ],
      chart: {
        fontFamily: 'Nunito Sans,sans-serif',
        height: 250,
        type: 'area',
        toolbar: {
          show: false
        }
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: 'smooth',
        width: '1',
      },
      grid: {
        strokeDashArray: 3,
      },

      xaxis: {
        categories: [
          "Jan",
          "Feb",
          "March",
          "April",
          "May",
          "June",
          "July",
          "Aug",
        ],
      },
      tooltip: {
        theme: 'dark'
      }
    };
  }

  ngOnInit(): void {
  }

}
