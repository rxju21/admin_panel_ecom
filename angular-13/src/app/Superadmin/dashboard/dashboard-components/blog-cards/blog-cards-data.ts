export interface blogcard {
    title: string,
    subtitle: string,
    subtext: string,
    image: string
}

export const blogcards: blogcard[] = [

    {
        title: 'This is simple blog',
        subtitle: '1 comments, 5 Like',
        subtext: 'This is a wider card with supporting text below as a natural lead-in to additional content.',
        image: 'assets/images/bg/bg1.jpg'
    },
    {
        title: 'This is simple blog',
        subtitle: '3 comments, 7 Like',
        subtext: 'This is a wider card with supporting text below as a natural lead-in to additional content.',
        image: 'assets/images/bg/bg2.jpg'
    },
    {
        title: 'This is simple blog',
        subtitle: '5 comments, 10 Like',
        subtext: 'This is a wider card with supporting text below as a natural lead-in to additional content.',
        image: 'assets/images/bg/bg3.jpg'
    },
    {
        title: 'This is simple blog',
        subtitle: '7 comments, 10 Like',
        subtext: 'This is a wider card with supporting text below as a natural lead-in to additional content.',
        image: 'assets/images/bg/bg4.jpg'
    },

] 