import { Component, AfterViewInit } from '@angular/core';
import {TranslateLoader, TranslateModule, TranslateService} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient, HttpClientModule} from '@angular/common/http';
//declare var require: any;

@Component({
  templateUrl: './dashboard.component.html'
})
export class DashboardComponent implements AfterViewInit {
  subtitle: string;
  constructor(public translate: TranslateService) {
    translate.setDefaultLang('en');
    this.subtitle = 'This is some text within a card block.';
  }

  ngAfterViewInit() { }
}
