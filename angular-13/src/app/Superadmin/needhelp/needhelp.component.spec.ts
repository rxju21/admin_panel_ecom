import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoNeedhelpComponent } from './needhelp.component';

describe('DemoNeedhelpComponent', () => {
  let component: DemoNeedhelpComponent;
  let fixture: ComponentFixture<DemoNeedhelpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DemoNeedhelpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoNeedhelpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
