import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { NeedhelpComponent } from "./needhelp.component";


const routes: Routes = [
  {
    path: "",
    data: {
      title: "Need Help?",
      urls: [{ title: "Need Help?", url: "/needhelp" }, { title: "Need Help?" }],
    },
    component: NeedhelpComponent,
  },
];

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    RouterModule.forChild(routes),
  ],
  declarations: [
    
  ],
})
export class DemoNeedhelpdemoModule_ {}
