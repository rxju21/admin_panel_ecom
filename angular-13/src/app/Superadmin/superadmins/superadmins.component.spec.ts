import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoSuperadminComponent } from './superadmins.component';

describe('DemoSuperadminComponent', () => {
  let component: DemoSuperadminComponent;
  let fixture: ComponentFixture<DemoSuperadminComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DemoSuperadminComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoSuperadminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
