import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { SuperadminsComponent } from "./superadmins.component";


const routes: Routes = [
  {
    path: "",
    data: {
      title: "Super Admins",
      urls: [{ title: "Super Admins", url: "/demo-superadmin" }, { title: "Super Admins" }],
    },
    component: SuperadminsComponent,
  },
];

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    RouterModule.forChild(routes),
  ],
  declarations: [
    
  ],
})
export class SuperadmindemoModule_ {}
