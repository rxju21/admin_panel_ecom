import { Component } from '@angular/core';
import { Product, TopSelling } from './superadmins.data';
import { defaultconfigdata } from '../../../environments/configdata';
import { HttpCall } from './../../services/httpcall.service'
import { httproute } from './../../services/consts';
// import { defaultconfigdata } from 'C:/Users/saura/Documents/Internship2022/ang/admin_panel_ecom/angular-13/src/environments/configdata';

@Component({
  selector: 'superadmins',
  templateUrl: './superadmins.component.html'
})
export class SuperadminsComponent {

  listofsadmins: any = defaultconfigdata.listofsadmin;

  topSelling: Product[];

  trow: Product[];

  constructor(public httpCall: HttpCall) {

    this.topSelling = TopSelling;

    this.trow = TopSelling;
    this.getData()
  }
  async getData() {
    let data = await this.httpCall.httpGetCall(httproute.GET_SUPERADMIN).toPromise()
    console.log("data========", data)
  }
}
