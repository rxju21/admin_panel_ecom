import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError, Subject } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { defaultconfigdata } from "../../environments/configdata";


@Injectable({ providedIn: 'root' })
export class HttpCall {
  observer_distributor = new Subject();
  public openmodeledit_distributor$ = this.observer_distributor.asObservable();
  saveEmit = new Subject();
  public saveEmit_$ = this.saveEmit.asObservable();
  LOCAL_OFFSET: any;
  constructor(private http: HttpClient,) {

  }

  public httpGetCall(userroute: any): Observable<any> {

    var url = defaultconfigdata.apiurl;
    return this.http.get(url + userroute, {}).pipe(
      map((res) => {
        return res;
      }),
      catchError(this.handleError));
  }

  public httpPostCall(userroute: any, userdata: any): Observable<any> {

    var url = defaultconfigdata.apiurl;
    return this.http.post(url + userroute, userdata, {}).pipe(
      map((res) => {
        return res;
      }),
      catchError(this.handleError));
  }

  public httpPutCall(userroute: any, userdata: any): Observable<any> {

    var url = defaultconfigdata.apiurl;
    return this.http.put(url + userroute, userdata, {}).pipe(
      map((res) => {
        return res;
      }),
      catchError(this.handleError));
  }
  public httpDeleteCall(userroute: any, userdata: any): Observable<any> {

    var url = defaultconfigdata.apiurl;
    return this.http.delete(url + userroute, userdata).pipe(
      map((res) => {
        return res;
      }),
      catchError(this.handleError));
  }

  public httpPostCallWithoutToken(userroute: any, userdata: any): Observable<any> {

    var url = defaultconfigdata.apiurl;
    return this.http.post(url + userroute, userdata).pipe(
      map((res) => {
        return res;
      }),
      catchError(this.handleError));
  }

  public getAllProject(): Observable<any> {


    var url = defaultconfigdata.apiurl;
    return this.http.get(url + "/webapi/v1/portal/getallprojects", {}).pipe(
      map((res) => {
        return res;
      }),
      catchError(this.handleError));
  }

  public getJSON(url: any): Observable<any> {
    return this.http.get(url);
  }

  handleError(error: any) {
    if (error.error instanceof Error) {
      let errMessage = error.error.message;
      return throwError(errMessage);
    }
    return throwError(error);
  }



}


