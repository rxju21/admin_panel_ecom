import { Routes } from '@angular/router';
/* <=======================================================================> */
/* <=======================================================================> */
//                Superadmins Portal Imports Only
/* <=======================================================================> */
/* <=======================================================================> */
import { loginportalcomponent } from './portal/loginportal/loginportal.component';
import { AboutusComponent } from './Superadmin/aboutus/aboutus.component';
import { DashboardComponent } from './Superadmin/dashboard/dashboard.component';
import { AddclientComponent } from './Superadmin/addclient/addclient.component';
import { AgentComponent } from './Superadmin/agent/agent.component';
import { ClientComponent } from './Superadmin/client/client.component';
import { NeedhelpComponent } from './Superadmin/needhelp/needhelp.component';
import { sitesettingsComponent } from './Superadmin/sitesettings/sitesettings.component';
import { SuperadminsComponent } from './Superadmin/superadmins/superadmins.component';
import { DistributorComponent } from './Superadmin/distributor/distributor.component';
import { FullComponent } from './Superadmin/layouts/full/full.component';
import { logincomponent } from './Superadmin/login_signup/login';
import { signupcomponent } from './Superadmin/login_signup/signup';
/* <=======================================================================> */
/* <=======================================================================> */
//                       Portal Imports Only
/* <=======================================================================> */
/* <=======================================================================> */
import { FullComponentPortal } from './portal/layouts/full/portalfull.component';
import { DashboardportalComponent } from './portal/dashboardportal/dashboardportal.component';
import { AboutportalComponent } from './portal/aboutportal/aboutportal.component';
import { SubjectPortalComponent } from './portal/subjectportal/subjectportal.component';
import { portalroutesComponent } from './portal/portalroutes/portalroutes.component';
import { portalfacilitiesComponent } from './portal/portalfacilities/portalfacilities.component';
import { portaldriversComponent } from './portal/portaldrivers/portaldrivers.component';
import { SiteSettingsportalComponent } from './portal/sitesettingsportal/sitesettingsportal.component';
import { PortalStaffComponent } from './portal/portalstaff/portalstaff.component';
import { PortalFeesComponent } from './portal/feesportal/feesportal.component';
import { PortalparentsComponent } from './portal/portalparents/portalparents.component';
import { TransportationportalComponent } from './portal/transportationportal/transportationportal.component';
import { AddparentComponent } from './portal/addparent/addparent.component';
import { PortalcalendarComponent } from './portal/portalcalendar/portalcalendar.component';
import { AcademicCircularsComponent } from './portal/academic-circulars/academic-circulars.component';
import { ImportantCircularsComponent } from './portal/important-circulars/important-circulars.component';
import { CocurricularCircularsComponent } from './portal/cocurricular-circulars/cocurricular-circulars.component';
import { StudymaterialComponent } from './portal/studymaterial/studymaterial.component';
import { ResultsComponent } from './portal/results/results.component';
import { AttendenceComponent } from './portal/attendence/attendence.component';
import { HomeworkComponent } from './portal/homework/homework.component';
import { QuotesComponent } from './portal/quotes/quotes.component';
import { TimetableComponent } from './portal/timetable/timetable.component';




export const Approutes: Routes = [
  { path: '', redirectTo: '/loginportal', pathMatch: 'full' },
  {
    path: '',
    component: FullComponentPortal,
    children: [
      {
        path: 'dashboardportal',
        component: DashboardportalComponent
      },
      {
        path: 'about',
        component: AboutportalComponent
      },
      {
        path: 'Routes',
        component: portalroutesComponent
      },
      {
        path: 'subjectportal',
        component: SubjectPortalComponent
      },
      {
        path: 'Facilities',
        component: portalfacilitiesComponent
      },
      {
        path: 'Drivers',
        component: portaldriversComponent
      },
      {
        path: 'sitesettings',
        component: SiteSettingsportalComponent
      },
      {
        path: 'portalstaff',
        component: PortalStaffComponent
      },
      {
        path: 'Fees',
        component: PortalFeesComponent
      },
      {
        path: 'transportationportal',
        component: TransportationportalComponent
      },
      {
        path: 'Parents',
        component: PortalparentsComponent
      },
      {
        path: 'StudyMaterial',
        component: StudymaterialComponent
      },
      {
        path: 'Results',
        component: ResultsComponent,
      },
      {
        path: 'Attendence',
        component: AttendenceComponent,
      },
      {
        path: 'AddParent',
        component: AddparentComponent
      },
      {
        path: 'portalcalendar',
        component: PortalcalendarComponent
      },
      {
        path: 'Academic_Circulars',
        component: AcademicCircularsComponent
      },
      {
        path: 'Important_Circulars',
        component: ImportantCircularsComponent
      },
      {
        path: 'Co-Curriclar_Circulars',
        component: CocurricularCircularsComponent
      },
      {
        path: 'Homework',
        component: HomeworkComponent
      },
      {
        path: 'DailyQuotes',
        component: QuotesComponent
      },
      {
        path: 'Timetable',
        component: TimetableComponent
      },
    ]
  },
  {
    path: 'superadmin', component: FullComponent, children: [
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'Aboutus',
        component: AboutusComponent
      },
      {
        path: 'Distributors',
        component: DistributorComponent
      },
      {
        path: 'Agents',
        component: AgentComponent
      },
      {
        path: 'Superadmins',
        component: SuperadminsComponent
      },
      {
        path: 'Clients',
        component: ClientComponent
      },
      {
        path: 'NeedHelp',
        component: NeedhelpComponent
      },
      {
        path: 'SiteSettings',
        component: sitesettingsComponent
      },
      {
        path: 'AddNewClients',
        component: AddclientComponent
      },
    ]
  },
  {
    path: 'loginportal',
    component: loginportalcomponent
  },
  {
    path: 'superadmin/login',
    component: logincomponent,
  },
  {
    path: 'superadmin/signup',
    component: signupcomponent
  },

  {
    path: '**',
    redirectTo: '/starter'
  }
]