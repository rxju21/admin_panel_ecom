import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Approutes } from './app-routing.module';
import { AppComponent } from './app.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { NgApexchartsModule } from 'ng-apexcharts';
import { DataTablesModule } from 'angular-datatables';
/* ===============================================================  */
// Superadmin Portal Imports
/* ===============================================================  */
import { AboutusComponent } from './Superadmin/aboutus/aboutus.component';
import { BlogCardsComponent } from './Superadmin/dashboard/dashboard-components/blog-cards/blog-cards.component';
import { FeedsComponent } from './Superadmin/dashboard/dashboard-components/feeds/feeds.component';
import { SalesSummaryComponent } from './Superadmin/dashboard/dashboard-components/sales-summary/sales-summary.component';
import { TopCardsComponent } from './Superadmin/dashboard/dashboard-components/top-cards/top-cards.component';
import { TopSellingComponent } from './Superadmin/dashboard/dashboard-components/top-selling/top-selling.component';
import { DashboardComponent } from './Superadmin/dashboard/dashboard.component';
import { AddclientComponent } from './Superadmin/addclient/addclient.component';
import { HttpLoaderFactory } from './Superadmin/addclient/addclient.module';
import { AgentComponent } from './Superadmin/agent/agent.component';
import { ClientComponent } from './Superadmin/client/client.component';
import { NeedhelpComponent } from './Superadmin/needhelp/needhelp.component';
import { sitesettingsComponent } from './Superadmin/sitesettings/sitesettings.component';
import { SuperadminsComponent } from './Superadmin/superadmins/superadmins.component';
import { DistributorComponent } from './Superadmin/distributor/distributor.component';
import { FullComponent } from './Superadmin/layouts/full/full.component';
import { logincomponent } from './Superadmin/login_signup/login';
import { signupcomponent } from './Superadmin/login_signup/signup';
import { NavigationComponent } from './Superadmin/shared/header/navigation.component';
import { SidebarComponent } from './Superadmin/shared/sidebar/sidebar.component';
import { SpinnerComponent } from './Superadmin/shared/spinner.component';
import { FullComponentPortal } from './portal/layouts/full/portalfull.component';
/* ===============================================================  */
// Portal Imports
/* ===============================================================  */
import { AboutportalComponent } from './portal/aboutportal/aboutportal.component';
import { PortalStaffComponent } from './portal/portalstaff/portalstaff.component';
import { SubjectPortalComponent } from './portal/subjectportal/subjectportal.component';
import { portalfacilitiesComponent } from './portal/portalfacilities/portalfacilities.component';
import { DashboardportalComponent } from './portal/dashboardportal/dashboardportal.component';
import { portalroutesComponent } from './portal/portalroutes/portalroutes.component';
import { portaldriversComponent } from './portal/portaldrivers/portaldrivers.component';
import { SiteSettingsportalComponent } from './portal/sitesettingsportal/sitesettingsportal.component';
import { PortalNavigationComponent } from './portal/shared/portalheader/portalnavigation.component';
import { SidebarPortalComponent } from './portal/shared/portalsidebar/portalsidebar.component';
import { PortalTopCardsComponent } from './portal/dashboardportal/dashboard-components/top-cards/top-cards.component';
import { PortalSalesSummaryComponent } from './portal/dashboardportal/dashboard-components/sales-summary/sales-summary.component';
import { PortalFeedsComponent } from './portal/dashboardportal/dashboard-components/feeds/feeds.component';
import { PortalTopSellingComponent } from './portal/dashboardportal/dashboard-components/top-selling/top-selling.component';
import { loginportalcomponent } from './portal/loginportal/loginportal.component';
import { PortalFeesComponent } from './portal/feesportal/feesportal.component';
import { TransportationportalComponent } from './portal/transportationportal/transportationportal.component';
import { PortalparentsComponent } from './portal/portalparents/portalparents.component';
import { PortalcalendarComponent } from './portal/portalcalendar/portalcalendar.component';
import { AddparentComponent } from './portal/addparent/addparent.component';
import { AcademicCircularsComponent } from './portal/academic-circulars/academic-circulars.component';
import { ImportantCircularsComponent } from './portal/important-circulars/important-circulars.component';
import { CocurricularCircularsComponent } from './portal/cocurricular-circulars/cocurricular-circulars.component';
import { StudymaterialComponent } from './portal/studymaterial/studymaterial.component';
import { ResultsComponent } from './portal/results/results.component';
import { AttendenceComponent } from './portal/attendence/attendence.component';
import { HomeworkComponent } from './portal/homework/homework.component';
import { QuotesComponent } from './portal/quotes/quotes.component';
import { TimetableComponent } from './portal/timetable/timetable.component';
import { FlatpickrModule } from 'angularx-flatpickr';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';


const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true,
  wheelSpeed: 1,
  wheelPropagation: true,
  minScrollbarLength: 20
};

@NgModule({
  declarations: [
    AppComponent,
    SpinnerComponent,
    FullComponent,
    FullComponentPortal,
    NavigationComponent,
    SidebarComponent,
    AboutusComponent,
    DistributorComponent,
    AgentComponent,
    SuperadminsComponent,
    ClientComponent,
    NeedhelpComponent,
    sitesettingsComponent,
    SalesSummaryComponent,
    AddclientComponent,
    FeedsComponent,
    TopSellingComponent,
    TopCardsComponent,
    BlogCardsComponent,
    DashboardComponent,
    logincomponent,
    signupcomponent,

    //Portal Imports only
    AboutportalComponent,
    PortalStaffComponent,
    SubjectPortalComponent,
    portalfacilitiesComponent,
    DashboardportalComponent,
    portalroutesComponent,
    loginportalcomponent,
    portaldriversComponent,
    SiteSettingsportalComponent,
    PortalNavigationComponent,
    SidebarPortalComponent,
    PortalTopCardsComponent,
    PortalSalesSummaryComponent,
    PortalFeedsComponent,
    PortalTopSellingComponent,
    PortalFeesComponent,
    TransportationportalComponent,
    PortalparentsComponent,
    AddparentComponent,
    PortalcalendarComponent,
    AcademicCircularsComponent,
    ImportantCircularsComponent,
    CocurricularCircularsComponent,
    StudymaterialComponent,
    ResultsComponent,
    AttendenceComponent,
    HomeworkComponent,
    QuotesComponent,
    TimetableComponent,


    
  ],
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule,
    NgApexchartsModule,
    FormsModule,
    RouterModule.forRoot(Approutes, { useHash: false, relativeLinkResolution: 'legacy' }),
    PerfectScrollbarModule,
    FormsModule,
    DataTablesModule,
    HttpClientModule,

    // ngx-translate and the loader module
    HttpClientModule,
    FlatpickrModule.forRoot(),
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory,
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      },
    })
  ],
  providers: [
    {
      provide: LocationStrategy,
      useClass: PathLocationStrategy
    },
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }