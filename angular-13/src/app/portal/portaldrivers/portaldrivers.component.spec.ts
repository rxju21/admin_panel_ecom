import { ComponentFixture, TestBed } from '@angular/core/testing';

import { portaldriversComponent } from './portaldrivers.component';

describe('DemoNeedhelpComponent', () => {
  let component: portaldriversComponent;
  let fixture: ComponentFixture<portaldriversComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ portaldriversComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(portaldriversComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
