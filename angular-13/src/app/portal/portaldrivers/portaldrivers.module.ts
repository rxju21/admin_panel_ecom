import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import {  portaldriversComponent } from "./portaldrivers.component";


const routes: Routes = [
  {
    path: "",
    data: {
      title: "Drivers",
      urls: [{ title: "Drivers", url: "/portaldrivers" }, { title: "Drivers" }],
    },
    component: portaldriversComponent,
  },
];

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    RouterModule.forChild(routes),
  ],
  declarations: [
    
  ],
})
export class DemodriversdemoModule_ {}
