import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';


@Component({
  selector: 'app-addparent',
  templateUrl: './addparent.component.html',
  styleUrls: ['./addparent.component.css']
})
export class AddparentComponent implements OnInit {
  parentinfo: FormGroup;
  submitted = false;

  constructor(private formBuilder: FormBuilder, private route: Router) { }

  ngOnInit(): void {
    this.parentinfo = this.formBuilder.group({
      parent_name: ['', Validators.required],
      mobile: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      relation: ['', Validators.required],
      
  });
  }
  get f() { return this.parentinfo.controls; }
  onSubmit() {

    this.submitted = true;
    // stop here if form is invalid
    if (this.parentinfo.invalid) {
        return;

    }
    else {
      Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })

        .fire({
            icon: 'success',
            title: "Parent Added Successfully!",
        })

    }
}
parent(){
  this.route.navigate(['/Parents'])}
}
