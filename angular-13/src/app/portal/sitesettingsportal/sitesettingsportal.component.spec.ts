import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SiteSettingsportalComponent } from './sitesettingsportal.component';

describe('DemoSitesettingsComponent', () => {
  let component: SiteSettingsportalComponent;
  let fixture: ComponentFixture<SiteSettingsportalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SiteSettingsportalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteSettingsportalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
