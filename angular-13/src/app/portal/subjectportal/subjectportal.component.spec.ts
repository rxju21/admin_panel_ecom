import { ComponentFixture, TestBed } from '@angular/core/testing';

import {  SubjectPortalComponent } from './subjectportal.component';

describe('DemoAgentComponent', () => {
  let component: SubjectPortalComponent;
  let fixture: ComponentFixture<SubjectPortalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubjectPortalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubjectPortalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
