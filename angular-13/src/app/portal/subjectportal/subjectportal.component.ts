import { Component } from '@angular/core';
import {Product,TopSelling,} from './subjectportal.data';
import { defaultconfigdata } from '../../../environments/configdata';


@Component({
    selector: 'app-table',
    templateUrl: './subjectportal.component.html'
})
export class SubjectPortalComponent {

  listofagent:any = defaultconfigdata.listofagent;


  topSelling:Product[];

  trow:Product[];

  constructor() { 

    this.topSelling=TopSelling;

    this.trow=TopSelling;
  }
}
