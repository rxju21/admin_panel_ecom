export interface Product {
    image: string,
    uname: string,
    gmail: string,
    productName: string,
    status: string,
    weeks: number,
    budget: string,
    fname: string,
    lname: string,
    u_id: string,
}

export interface TableRows {
    
}

export const TopSelling: Product[] = [

    {
        image: 'assets/images/users/user1.jpg',
        uname: 'Sanjeet Nikam',
        gmail: 'srnikam@gmail.com',
        productName: 'Flexy React',
        status: 'danger',
        weeks: 35,
        budget: '95K',
        fname: "Sanjeet",
        lname: "Nikam",
        u_id: "@sanjeet.nikam",
    },
    {
        image: 'assets/images/users/user2.jpg',
        uname: 'Saurabh Shah',
        gmail: 'smshah@gmail.com',
        productName: 'Landing pro React',
        status: 'info',
        weeks: 35,
        budget: '95K',
        fname: "Saurabh",
        lname: "Shah",
        u_id: "@_saurabhshah",
    },
    {
        image: 'assets/images/users/user3.jpg',
        uname: 'Rohit Kori',
        gmail: 'rbkori@gmail.com',
        productName: 'Elite React',
        status: 'warning',
        weeks: 35,
        budget: '95K',
        fname: "Rohit",
        lname: "Kori",
        u_id: "@rohit_kori",
    },
    {
        image: 'assets/images/users/user4.jpg',
        uname: 'Ashneer Grover',
        gmail: 'agrover@gmail.com',
        productName: 'Ample React',
        status: 'success',
        weeks: 35,
        budget: '95K',
        fname: "Ashneer",
        lname: "Grover",
        u_id: "@theashneer_grover",
    },

]