import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { AgentPortalComponent } from "./subjectportal.component";


const routes: Routes = [
  {
    path: "",
    data: {
      title: "Agents",
      urls: [{ title: "Agents", url: "/demo-agent" }, { title: "Agents" }],
    },
    component: AgentPortalComponent,
  },
];

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    RouterModule.forChild(routes),
  ],
  declarations: [
    
  ],
})
export class AgentdemoModule_ {}
