import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AcademicCircularsComponent } from './academic-circulars.component';

describe('AcademicCircularsComponent', () => {
  let component: AcademicCircularsComponent;
  let fixture: ComponentFixture<AcademicCircularsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AcademicCircularsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AcademicCircularsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
