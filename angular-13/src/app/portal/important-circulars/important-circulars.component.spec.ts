import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportantCircularsComponent } from './important-circulars.component';

describe('ImportantCircularsComponent', () => {
  let component: ImportantCircularsComponent;
  let fixture: ComponentFixture<ImportantCircularsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImportantCircularsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportantCircularsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
