import { Component, AfterViewInit } from '@angular/core';
import { TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-dashboardpor',
  templateUrl: './dashboardportal.component.html',
})
export class DashboardportalComponent implements AfterViewInit {

  subtitle: string;
  constructor(public translate: TranslateService) {
    translate.setDefaultLang('en');
    this.subtitle = 'This is some text within a card block.';
  }

  ngAfterViewInit() { }
}
