import { Component, OnInit } from '@angular/core';
import { Feeds,Feed } from './feeds-data';
import { defaultconfigdata } from '../../../../../environments/configdata';
// import { defaultconfigdata } from 'C:/Users/saura/Documents/Internship2022/ang/admin_panel_ecom/angular-13/src/environments/configdata';

@Component({
  selector: 'portal-app-feeds',
  templateUrl: './feeds.component.html'
})
export class PortalFeedsComponent implements OnInit {

    dailyact: any = defaultconfigdata.dailyact;
    dailynot: any = defaultconfigdata.dailynot;


  feeds:Feed[];

  constructor() {

    this.feeds = Feeds;
  }

  ngOnInit(): void {
  }

}
