import { Component, OnInit, ViewChild } from '@angular/core';
import {
  ApexAxisChartSeries,
  ApexChart,
  ChartComponent,
  ApexDataLabels,
  ApexYAxis,
  ApexLegend,
  ApexXAxis,
  ApexTooltip,
  ApexTheme,
  ApexGrid
} from 'ng-apexcharts';
import { defaultconfigdata } from '../../../../../environments/configdata';
import {TranslateLoader, TranslateModule, TranslateService} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient, HttpClientModule} from '@angular/common/http';
// import { defaultconfigdata } from 'C:/Users/saura/Documents/Internship2022/ang/admin_panel_ecom/angular-13/src/environments/configdata';

export type salesChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  yaxis: ApexYAxis;
  stroke: any;
  theme: ApexTheme;
  tooltip: ApexTooltip;
  dataLabels: ApexDataLabels;
  legend: ApexLegend;
  colors: string[];
  markers: any;
  grid: ApexGrid;
};

@Component({
  selector: 'portal-app-sales-summary',
  templateUrl: './sales-summary.component.html'
})
export class PortalSalesSummaryComponent implements OnInit {
  
  salessummary: any = defaultconfigdata.salessummary;
  yearlysalessummary: any = defaultconfigdata.yrsalessummary;
  monthlysalessummary: any = defaultconfigdata.monsalessummary;
  weeklysalessummary: any = defaultconfigdata.weeksalessummary;
  dailysalessummary: any = defaultconfigdata.dailysalessummary;

  @ViewChild("chart") chart: ChartComponent = Object.create(null);
  public salesChartOptions: Partial<salesChartOptions>;
  
  
  constructor(public translate: TranslateService) { 
    translate.setDefaultLang('en');
    
  
    this.salesChartOptions = {
      series: [
        {
          name: "English",
          data: [0, 40, 45, 0, 90, 0, 41, 0,0,0,97,0],
        },
        {
          name: "Maths",
          data: [0, 45, 44, 0, 80, 0, 40, 0,0,0,90,0],
        },
        {
          name: "Science",
          data: [0, 47, 43, 0, 89, 0, 39, 0,0,0,89,0],
        },
        {
          name: "Social Science",
          data: [0, 49, 25, 0, 87, 0, 38, 0,0,0,85,0],
        },
        {
          name: "Art",
          data: [0, 35, 20, 0, 70, 0, 40, 0,0,0,50,0],
        },
        {
          name: "Hindi",
          data: [0, 39, 30, 0, 75, 0, 42, 0,0,0,97,0],
        },
        {
          name: "Gujarati",
          data: [0, 37, 50, 0, 92, 0, 43, 0,0,0,98,0],
        },
      ],
      chart: {
        fontFamily: 'Nunito Sans,sans-serif',
        height: 250,
        type: 'area',
        toolbar: {
          show: false
        }
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: 'smooth',
        width: '1',
      },
      grid: {
        strokeDashArray: 3,
      },

      xaxis: {
        categories: [
          "Jan",
          "Feb",
          "March",
          "April",
          "May",
          "June",
          "July",
          "Aug",
          "Sept",
          "Oct",
          "Nov",
          "Dec"
        ],
      },
      tooltip: {
        theme: 'dark'
      }
    };
  }

  ngOnInit(): void {
  }

}
