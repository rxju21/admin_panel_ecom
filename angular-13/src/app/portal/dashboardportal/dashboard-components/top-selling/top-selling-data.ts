import { sitesettingsComponent } from './../../../../Superadmin/sitesettings/sitesettings.component';
export interface Book {
    bookid : string,
    bookname: string,
    status: string,
    author: string,
    issued_date: string,
    returned_date: string,
    remarks: string,
}

export const TopSelling: Book[] = [

    {
        bookid :'CE1001',
        bookname: 'Computer Graphics',
        status: 'warning',
        author: 'Arjun Ramaswamy',
        issued_date: '12-12-2020',
        returned_date: "16-12-2020",
        remarks: "Pages were Missing",
    },
    {
        bookid :'CE1546',
        bookname: 'Web Devlopment',
        status: 'success',
        author: 'Arjun Shah',
        issued_date: '06-03-2021',
        returned_date: "30-03-2021",
        remarks: "N/A",
    },
    {
        bookid :'CE1023',
        bookname: 'Java',
        status: 'danger',
        author: 'Stephen Chadwik',
        issued_date: '15-05-2021',
        returned_date: "01-06-2021",
        remarks: "Returned Late",
    },
    {
        bookid :'CE1056',
        bookname: 'Android Development',
        status: 'info',
        author: 'Sundar Ramnujan',
        issued_date: '17-07-2021',
        returned_date: "Return in Progress",
        remarks: "Facing Technical Issue",
    },

]