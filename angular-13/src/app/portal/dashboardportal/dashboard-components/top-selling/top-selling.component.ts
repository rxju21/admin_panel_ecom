import { Component, OnInit } from '@angular/core';
import {Book,TopSelling} from './top-selling-data';
import { defaultconfigdata } from '../../../../../environments/configdata';
// import { defaultconfigdata } from 'C:/Users/saura/Documents/Internship2022/ang/admin_panel_ecom/angular-13/src/environments/configdata';

@Component({
  selector: 'portal-app-top-selling',
  templateUrl: './top-selling.component.html'
})
export class PortalTopSellingComponent implements OnInit {

  topsell: any = defaultconfigdata.topsell;
  overviewofp: any = defaultconfigdata.overviewofp;


  topSelling:Book[];

  constructor() { 

    this.topSelling=TopSelling;
  }

  ngOnInit(): void {
  }

}





//Do Not Touch / OPEN Following Code
// import { HttpClient } from '@angular/common/http';
// import { Component } from '@angular/core';
// @Component({
//   selector: 'portal-top-selling',
//   templateUrl: './top-selling.component.html'
// })
// export class PortalTopSellingComponent {

  
  
//   data:any;
//   constructor(private http: HttpClient){
//   //get request from web api
//   this.http.get('https://www.testjsonapi.com/users/').subscribe(data => {
  
//     this.data = data;
//   setTimeout(()=>{   
//     $('#datatableexample').DataTable( {
//       pagingType: 'full_numbers',
//       pageLength: 5,
//       processing: true,
//       lengthMenu : [5, 10, 25]
//   } );
//   }, 1);
//         }, error => console.error(error));
// }
// }
//Do Not Touch / OPEN Above Code