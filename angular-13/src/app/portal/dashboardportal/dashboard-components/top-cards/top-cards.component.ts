import { topcard } from './top-cards-data';
import { Component, OnInit } from '@angular/core';
import { defaultconfigdata } from '../../../../../environments/configdata';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-portal-top-cards',
  templateUrl: './top-cards.component.html'
})
export class PortalTopCardsComponent implements OnInit {

  inactive_users: any = defaultconfigdata.Inactive_users_label;
  active_users: any = defaultconfigdata.Active_users_label;
  total_users: any = defaultconfigdata.Total_users_label;
  distributors: any = defaultconfigdata.Distributors_label;
  agents: any = defaultconfigdata.Agents_label;
  totalsales: any = defaultconfigdata.Total_Sales_label;

  topcards: topcard[];

  constructor(public translate: TranslateService) {
    // this.translate.stream(['']).subscribe((textarray) => 
    // {
    //   this.topcards = [
 
    //     {
    //       bgcolor: 'success',
    //       icon: 'bi bi-person-check-fill',
    //       title: "Good Job, Sarah. Keep Going!!",
    //       // subtitle: this.translate.instant('topcards_dashboard.title'),
    //       subtitle: "You task are 80% completed this week. Keep it up and improve your result. Progress is very good!!!",
    //   },

    //   ];
    // }
  };

  ngOnInit(): void {
  }

}
