import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";
import { NgApexchartsModule } from "ng-apexcharts";
import { DashboardportalComponent } from "./dashboardportal.component";

const routes: Routes = [
  {
    path: "",
    data: {
      title: "Dashboard",
      urls: [{ title: "Dashboard", url: "/dashboardpor.component" }, { title: "Dashboard" }],
    },
    component: DashboardportalComponent,
  },
];

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    RouterModule.forChild(routes),
    NgApexchartsModule,
  ],
  declarations: [
    // DashboardComponent,
    // SalesSummaryComponent,
    // FeedsComponent,
    // TopSellingComponent,
    // TopCardsComponent,
    // BlogCardsComponent
  ],
})
export class DashboardporModule {}
