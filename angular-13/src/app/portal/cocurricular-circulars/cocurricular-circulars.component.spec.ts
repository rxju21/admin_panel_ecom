import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CocurricularCircularsComponent } from './cocurricular-circulars.component';

describe('CocurricularCircularsComponent', () => {
  let component: CocurricularCircularsComponent;
  let fixture: ComponentFixture<CocurricularCircularsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CocurricularCircularsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CocurricularCircularsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
