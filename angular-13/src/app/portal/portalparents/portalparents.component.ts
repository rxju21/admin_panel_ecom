import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-portalparents',
  templateUrl: './portalparents.component.html',
  styleUrls: ['./portalparents.component.css']
})
export class PortalparentsComponent implements OnInit {

  constructor(private route: Router) { }

  ngOnInit(): void {
  }
 addparent(){
  this.route.navigate(['/AddParent'])
 }
}
