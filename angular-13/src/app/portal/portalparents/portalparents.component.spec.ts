import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PortalparentsComponent } from './portalparents.component';

describe('PortalparentsComponent', () => {
  let component: PortalparentsComponent;
  let fixture: ComponentFixture<PortalparentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PortalparentsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PortalparentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
