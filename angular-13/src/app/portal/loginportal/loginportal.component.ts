import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';



@Component({
    selector: 'portal-login',
    templateUrl: './loginportal.component.html',
    styleUrls: ['../../Superadmin/login_signup/css/style.css']
})
export class loginportalcomponent implements OnInit {

    loginporval: FormGroup;
    postId: any;
    inputValue: string = '';
    submitted = false;

    public customPatterns = { '0': { pattern: new RegExp('\[a-zA-Z\]') } };

    constructor(private http: HttpClient, private formBuilder: FormBuilder, public translate: TranslateService, private route: Router) {
        translate.setDefaultLang('en');
    }
    ngOnInit() {

        

        this.loginporval = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.minLength(6)]],
            compcode: ['', Validators.required],

        });

    }
    addpprefix(event: any) {
        this.inputValue = 'S-';

    }

    // convenience getter for easy access to form fields
    get f() { return this.loginporval.controls; }
    onSubmitportal() {



        this.submitted = true;

        // stop here if form is invalid
        if (this.loginporval.invalid) {
            return;
        }

        // display form values on success
        this.route.navigate(['/dashboardportal']);
    }

}