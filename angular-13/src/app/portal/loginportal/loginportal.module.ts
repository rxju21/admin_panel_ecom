import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";


// import ngx-translate and the http loader
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import { loginportalcomponent } from "./loginportal.component";
import { IConfig, NgxMaskModule } from "ngx-mask";


const routes: Routes = [
  {
    path: "",
    data: {
      title: "LoginPortal",
      urls: [{ title: "LoginPortal", url: "/loginportal.component" }, { title: "LoginPortal" }],
    },
    component: loginportalcomponent,
  },
];
const maskConfig: Partial<IConfig> = {
  validation: false,
};

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    RouterModule.forChild(routes),
    NgxMaskModule.forRoot(maskConfig),

    // ngx-translate and the loader module
    HttpClientModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            },
        })
  ],
  declarations: [
    
  ],
})
export class loginportalModule_ {}

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient): TranslateHttpLoader {
    return new TranslateHttpLoader(http);
}