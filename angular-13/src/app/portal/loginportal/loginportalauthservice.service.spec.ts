import { TestBed } from '@angular/core/testing';

import { LoginportalauthserviceService } from './loginportalauthservice.service';

describe('LoginportalauthserviceService', () => {
  let service: LoginportalauthserviceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LoginportalauthserviceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
