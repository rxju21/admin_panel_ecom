import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginportalauthserviceService {
  private _loginUrl = "https://ci-managementtool.000webhostapp.com/superadmin/superadmin/login_superadmin.php"
  constructor(private http: HttpClient) { }

  loginUser(user: any) {
     return this.http.post<any>(`this._loginUrl`, user);
  }
}
