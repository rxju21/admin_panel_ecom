import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PortalcalendarComponent } from './portalcalendar.component';

describe('PortalcalendarComponent', () => {
  let component: PortalcalendarComponent;
  let fixture: ComponentFixture<PortalcalendarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PortalcalendarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PortalcalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges(); PortalcalendarComponent
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
