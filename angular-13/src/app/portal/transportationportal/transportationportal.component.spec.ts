import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransportationportalComponent } from './transportationportal.component';

describe('TransportationportalComponent', () => {
  let component: TransportationportalComponent;
  let fixture: ComponentFixture<TransportationportalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TransportationportalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TransportationportalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
