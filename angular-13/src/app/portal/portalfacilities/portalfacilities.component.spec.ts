import { ComponentFixture, TestBed } from '@angular/core/testing';

import { portalfacilitiesComponent } from './portalfacilities.component';

describe('DemoClientComponent', () => {
  let component: portalfacilitiesComponent;
  let fixture: ComponentFixture<portalfacilitiesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ portalfacilitiesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(portalfacilitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
