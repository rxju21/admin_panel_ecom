import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { portalfacilitiesComponent } from "./portalfacilities.component";


const routes: Routes = [
  {
    path: "",
    data: {
      title: "Facilities",
      urls: [{ title: "Facilities", url: "/Facilities" }, { title: "Facilities" }],
    },
    component: portalfacilitiesComponent,
  },
];

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    RouterModule.forChild(routes),
  ],
  declarations: [
    
  ],
})
export class ClientdemoModule_ {}
