import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Product, TopSelling } from './portalfacilities.data';
import { defaultconfigdata } from '../../../environments/configdata';



@Component({
  selector: 'app-client',
  templateUrl: './portalfacilities.component.html'
})
export class portalfacilitiesComponent {

  
  listofclients: any = defaultconfigdata.listofclients;

  topSelling: Product[];

  trow: Product[];

  constructor(private route: Router) {

    this.topSelling = TopSelling;

    this.trow = TopSelling;
  }
}
