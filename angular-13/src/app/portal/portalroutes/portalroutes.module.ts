import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import {  portalroutesComponent } from "./portalroutes.component";


const routes: Routes = [
  {
    path: "",
    data: {
      title: "Routes",
      urls: [{ title: "Routes", url: "/Routes" }, { title: "Routes" }],
    },
    component: portalroutesComponent,
  },
];

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    RouterModule.forChild(routes),
  ],
  declarations: [
    
  ],
})
export class demoModule_ {}
