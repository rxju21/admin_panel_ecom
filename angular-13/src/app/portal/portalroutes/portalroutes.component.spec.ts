import { ComponentFixture, TestBed } from '@angular/core/testing';

import { portalroutesComponent } from './portalroutes.component';

describe('DemoComponent', () => {
  let component: portalroutesComponent;
  let fixture: ComponentFixture<portalroutesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ portalroutesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(portalroutesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
