export interface Product {
    image: string,
    uname: string,
    gmail: string,
    productName: string,
    status: string,
    weeks: number,
    budget: string,
    fname: string,
    lname: string,
    u_id: string,
}

export interface TableRows {
    
}

export const TopSelling: Product[] = [

    {
        image: 'assets/images/users/user1.jpg',
        uname: 'Nikunj Kansara',
        gmail: 'nikunj.kansara@gmail.com',
        productName: 'Flexy React',
        status: 'danger',
        weeks: 35,
        budget: '95K',
        fname: "Nikunj",
        lname: "Kansara",
        u_id: "@nikunj.kansara",
    },
    {
        image: 'assets/images/users/user2.jpg',
        uname: 'Hanna Gover',
        gmail: 'hgover@gmail.com',
        productName: 'Landing pro React',
        status: 'info',
        weeks: 35,
        budget: '95K',
        fname: "Dharmishtha",
        lname: "Chaudhari",
        u_id: "@_cdharmishtha",
    },
    {
        image: 'assets/images/users/user3.jpg',
        uname: 'Madhavi Desai',
        gmail: 'hgover@gmail.com',
        productName: 'Elite React',
        status: 'warning',
        weeks: 35,
        budget: '95K',
        fname: "Madhavi",
        lname: "Desai",
        u_id: "@thehod_mbd",
    },
    {
        image: 'assets/images/users/user4.jpg',
        uname: 'Dhaval Rana',
        gmail: 'hgover@gmail.com',
        productName: 'Ample React',
        status: 'success',
        weeks: 35,
        budget: '95K',
        fname: "Dhaval",
        lname: "Rana",
        u_id: "@dhaval_rana",
    },

]