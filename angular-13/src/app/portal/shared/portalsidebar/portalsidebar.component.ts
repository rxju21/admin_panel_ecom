import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

declare var $: any;

@Component({
  selector: 'app-portal-sidebar',
  styleUrls: ['./portalsidebar.component.css'],
  templateUrl: './portalsidebar.component.html'
})
export class SidebarPortalComponent implements OnInit {


  constructor(private modalService: NgbModal, private route: Router) { }


  // End open close
  ngOnInit() { }

  ngAfterViewInit() { }

  dashboardportal() {
    console.log("Dashboard Clicked")
    this.route.navigate(['/dashboardportal'])
  }
  staff() {
    this.route.navigate(['/portalstaff'])
  }
  subject() {
    this.route.navigate(['/subjectportal'])
  }

  driver() {
    this.route.navigate(['/Drivers'])
  }
  routes() {
    this.route.navigate(['/Routes'])
  }
  facilities() {
    this.route.navigate(['/Facilities'])
  }
  fees() {
    this.route.navigate(['/Fees'])
  }
  transportation() {
    this.route.navigate(['/transportationportal']) 
  }
  studymaterial() {
    this.route.navigate(['/StudyMaterial'])
  }
  results() {
    this.route.navigate(['/Results'])
  }
  parents() {
    this.route.navigate(['/Parents'])
  }
  attendence() {
    this.route.navigate(['/Attendence']) 
  }
  homework() {
    this.route.navigate(['/Homework']) 
  }
  timetable() {
    this.route.navigate(['/Timetable']) 
  }
  portalcalendar() {
    this.route.navigate(['/portalcalendar'])
  }
  academic() {
    this.route.navigate(['/Academic_Circulars'])
  }
  important() {
    this.route.navigate(['/Important_Circulars'])
  }
  co_curricular() {
    this.route.navigate(['/Co-Curriclar_Circulars'])
  }
  dailyquotes() {
    this.route.navigate(['/DailyQuotes'])
  }
  settings() {
    this.route.navigate(['/sitesettings'])
  }
}