import { ComponentFixture, TestBed } from '@angular/core/testing';

import {  PortalStaffComponent } from './portalstaff.component';

describe('DemoAddclientComponent', () => {
  let component: PortalStaffComponent;
  let fixture: ComponentFixture<PortalStaffComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PortalStaffComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PortalStaffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
