import { Component, OnInit, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { defaultconfigdata } from '../../../environments/configdata';
import Stepper from 'bs-stepper';
import Swal from 'sweetalert2';
// import ngx-translate and the http loader
import {TranslateLoader, TranslateModule, TranslateService} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient, HttpClientModule} from '@angular/common/http';

// import custom validator to validate that password and confirm password fields match


@Component({
    selector: 'portal-staff',
    templateUrl: './portalstaff.component.html',
})
export class PortalStaffComponent implements AfterViewInit {

    //stepper1 File Constant starts
    stepper1_label: any = defaultconfigdata.stepper1_label;
    company_name: any = defaultconfigdata.company_name;
    website: any = defaultconfigdata.website;
    mobile: any = defaultconfigdata.mobile;
    phone: any = defaultconfigdata.phone;
    email: any = defaultconfigdata.email;
    fax: any = defaultconfigdata.fax;
    type_of_company: any = defaultconfigdata.type_of_company;
    company_active_since: any = defaultconfigdata.company_active_since;
    company_size: any = defaultconfigdata.company_size;
    division: any = defaultconfigdata.division;
    //stepper1 File Constant ends

    //stepper2 File Constant starts
    stepper2_label: any = defaultconfigdata.stepper2_label;
    company_status: any = defaultconfigdata.company_status;
    active: any = defaultconfigdata.active;
    inactive: any = defaultconfigdata.inactive;
    address: any = defaultconfigdata.address;
    state: any = defaultconfigdata.state;
    zip_code: any = defaultconfigdata.zip_code;
    country: any = defaultconfigdata.country;
    whatsapp: any = defaultconfigdata.whatsapp;
    twitter: any = defaultconfigdata.twitter;
    instagram: any = defaultconfigdata.instagram;
    facebook: any = defaultconfigdata.facebook;
    //stepper2 File Constant ends

    //stepper3 File Constant starts
    stepper3_label: any = defaultconfigdata.stepper3_label;
    contact_person_name: any = defaultconfigdata.contact_person_name;
    contact_person_title: any = defaultconfigdata.contact_person_title;
    contact_person_email: any = defaultconfigdata.contact_person_email;
    contact_person_mobile: any = defaultconfigdata.contact_person_mobile;
    contact_person_phone: any = defaultconfigdata.contact_person_phone;
    contact_person_whatsapp: any = defaultconfigdata.contact_person_whatsapp;
    contact_person_twitter: any = defaultconfigdata.contact_person_twitter;
    contact_person_instagram: any = defaultconfigdata.contact_person_instagram;
    contact_person_facebook: any = defaultconfigdata.contact_person_facebook;
    //stepper3 File Constant ends 

    //stepper4 File Constant starts
    stepper4_label: any = defaultconfigdata.stepper4_label;
    select_plan: any = defaultconfigdata.select_plan;
    basic: any = defaultconfigdata.basic;
    standard: any = defaultconfigdata.standard;
    pro: any = defaultconfigdata.pro;
    executive: any = defaultconfigdata.executive;
    enterprise: any = defaultconfigdata.enterprise;
    billing_person_name: any = defaultconfigdata.billing_person_name;
    billing_person_email: any = defaultconfigdata.billing_person_email;
    billing_person_mobile: any = defaultconfigdata.billing_person_mobile;
    billing_person_phone: any = defaultconfigdata.billing_person_phone;
    billing_person_address: any = defaultconfigdata.billing_person_address;
    billing_person_city: any = defaultconfigdata.billing_person_city;
    billing_person_state: any = defaultconfigdata.billing_person_state;
    billing_person_zip_code: any = defaultconfigdata.billing_person_zip_code;
    //stepper4 File Constant ends

    //Buttons in steppers File Constant starts
    Next: any = defaultconfigdata.Next;
    Previous: any = defaultconfigdata.Previous;
    submit: any = defaultconfigdata.submit;
    //Buttons in steppers File Constant ends
    public stepper!: Stepper;
    @ViewChild('bsStepper', { static: false }) stepperElement!: ElementRef<any>;

    companyinfo: FormGroup;
    address_info: FormGroup;
    contactperson_info: FormGroup;
    billing_info: FormGroup;

    submitted = false;

    constructor(private formBuilder: FormBuilder, public translate: TranslateService) { 
      translate.setDefaultLang('en');
    }

    ngOnInit() {
        this.companyinfo = this.formBuilder.group({
            company_name: ['', Validators.required],
            website: [''],
            mobile: ['', Validators.required],
            phone: [''],
            email: ['', [Validators.required, Validators.email]],
            fax: [''],
            company_type: [''],
            company_active: ['', Validators.required],
            company_size: [''],
            company_division: [''],
        });

        this.address_info = this.formBuilder.group({
            company_status: ['', Validators.required],
            address: [],
            state: ['', Validators.required],
            zip_code: ['', Validators.required],
            country: ['', Validators.required],
            whatsapp: [],
            twitter: [],
            instagram: [],
            facebook: [],
        });

        this.contactperson_info = this.formBuilder.group({
            contactperson_name: ['', Validators.required],
            contactperson_title: [''],
            contactperson_mobile: ['', Validators.required],
            contactperson_phone: [''],
            contactperson_email: ['', [Validators.required, Validators.email]],
            contactperson_whatsapp: [''],
            contactperson_twitter: [''],
            contactperson_instagram: [''],
            contactperson_facebook: [''],
        });

        this.billing_info = this.formBuilder.group({
            billing_selectplan: ['', Validators.required],
            billingperson_name: ['', Validators.required],
            billingperson_mobile: ['', Validators.required],
            billingperson_email: ['', [Validators.required, Validators.email]],
            billingperson_address: [''],
            billingperson_city: [''],
            billingperson_state: [''],
            billingperson_zipcode: [''],
        });
    }
    ngAfterViewInit() {
        this.stepper = new Stepper(this.stepperElement.nativeElement, {
            linear: true,
            animation: false,
        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.companyinfo.controls; }
    get g() { return this.address_info.controls; }
    get h() { return this.contactperson_info.controls; }
    get i() { return this.billing_info.controls; }

    onSubmit() {

        this.submitted = true;
        // stop here if form is invalid
        if (this.companyinfo.invalid) {
            return;

        }
        else {
            console.log(this.stepper);
            this.stepper.next();
        }
    }
    onSubmit2() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.address_info.invalid) {
            return;
        }

        else {
            console.log(this.stepper);
            this.stepper.next();
        }
    }
    onSubmit3() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.contactperson_info.invalid) {
            return;
        }

        else {
            console.log(this.stepper);
            this.stepper.next();
        }
    }
    onSubmit4() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.billing_info.invalid) {
            return;
        }

        else {
            Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                didOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            })

                .fire({
                    icon: 'success',
                    title: "Client Created Successfully!",
                })

        }

    }
    
    previous() {
        this.stepper.previous();
    }
}
